# minikube mount <source directory>:<target directory>
# minikube mount $HOME:/host    # $HOME = my linux-sys
minikube mount /data/jenkins-volume:/data/jenkins-volume


# Important: yaml files are to be run sequencial
#  to make previous step resources are available for the next steps

# (optional): only if 'jenkins-production' previously exists
# delete namespace 'jenkins-production'
kubectl delete namespace jenkins-production

# create namespace from yaml file
kubectl apply -f jenkins-namespace.yaml

# check the all the namespaces
kubectl get all --all-namespaces

# apply deployment from yaml file
kubectl apply -f jenkins-deployment.yaml

# check the all the namespaces, pod creation its status
kubectl get all --all-namespaces

# apply services from yaml file
kubectl apply -f jenkins-service.yaml

# check all entities running
kubectl get all --all-namespaces

# get the minikube-ip-address
minikube ip
# o/p : 192.168.49.2

# jenkins is available in the below minikube-ip-address
# e.g.: 192.168.49.2:32000


# get the pod in running in different namespace(jenkins-production)
kubectl get pods -n=jenkins-production

# get the jenkins initial password 
# Here, jenkins-production is namespace
# if explicity namespace is not mentioned, then default namespace is selected
kubectl logs <pod_name> -n jenkins-production

# kubectl logs <pod_name> -n <namespace>
