
$ kubectl get nodes
$ kubectl get pod
$ kubectl get services


$ kubectl create -h
# creation of pod happens on creation of deployment
$ kubectl create deployment nginx-depl --image=nginx

$ kubectl get deployment

# get replica set
$ kubectl get replicaset

$ kubectl edit deployment nginx-depl

$ kubectl logs <pod-name>
$ kubectl describe pod <pod-name>

# get into the container
$ kubectl exec -it <pod-name> -- bin/bash

# Deleting deployment
$ kubectl get deployment
$ kubectl get pod
$ kubectl delete deployment <deployment-name>

# deployment using config file
$ kubectl apply -f <yaml/json file-name>
# e.g. $ kubectl apply -f nginx-depl.yaml

# prints the worker nodes 
$ kubectl get nodes

# below 2 commands dont work properly use apply instead
# $ kubectl create -f jenkins-deployment.yaml -n jenkins
# $ kubectl create -f jenkins-service.yaml -n jenkins

$ kubectl apply -f jenkins-namespace.yaml
$ kubectl apply -f jenkins-volume.yaml
$ kubectl apply -f jenkins-deployment.yaml
$ kubectl apply -f jenkins-service.yaml

$ kubectl get deployments -n jenkins
$ kubectl get all --all-namespaces
$ kubectl delete pod jenkins
$ kubectl get services -n jenkins
$ kubectl logs <pod_name> -n jenkins
$ kubectl delete --all namespaces
$ kubectl describe <k8s-element>